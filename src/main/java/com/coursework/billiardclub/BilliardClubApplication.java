package com.coursework.billiardclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class BilliardClubApplication {

    public static void main(String[] args) {
        SpringApplication.run(BilliardClubApplication.class, args);
    }
}


package com.coursework.billiardclub.client;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String address;
    private String phone;
    private String email;
    private Boolean regular;
}

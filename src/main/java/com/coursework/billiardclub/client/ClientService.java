package com.coursework.billiardclub.client;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ClientService {
    private final ClientRepository clientRepository;

    public Client getOrderById(Long id) throws Exception {
        return clientRepository.findById(id).orElseThrow(Exception::new);
    }

    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }
}

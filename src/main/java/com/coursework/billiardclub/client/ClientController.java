package com.coursework.billiardclub.client;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@CrossOrigin
@RequestMapping("/api/clients")
public class ClientController {
    private final ClientService clientService;

    @GetMapping("/{id}")
    public Client getClientById(@PathVariable Long id) throws Exception {
        return clientService.getOrderById(id);
    }

    @GetMapping
    public List<Client> getAllClients() {
        List<Client> allClients = clientService.getAllClients();
        return allClients;
    }
}

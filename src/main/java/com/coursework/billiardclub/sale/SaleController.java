package com.coursework.billiardclub.sale;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/sales")
public class SaleController {
    private final SaleService saleService;

    @GetMapping("/{id}")
    public Sale getSaleById(@PathVariable Long id) throws Exception {
        return saleService.getSaleById(id);
    }
}

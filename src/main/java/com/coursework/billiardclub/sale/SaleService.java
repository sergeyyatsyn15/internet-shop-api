package com.coursework.billiardclub.sale;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class SaleService {
    private final SaleRepository saleRepository;

    public Sale getSaleById(Long id) throws Exception {
        return saleRepository.findById(id).orElseThrow(Exception::new);
    }
}

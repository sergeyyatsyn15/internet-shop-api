package com.coursework.billiardclub.sale;

import com.coursework.billiardclub.client.Client;
import com.coursework.billiardclub.goods.Goods;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @ManyToOne
    private Client client;
    private LocalDateTime sellDatetime;
    private LocalDateTime deliveryDate;
    private Long amount;
    @OneToMany(mappedBy = "id", fetch = FetchType.EAGER)
    private List<Goods> goods;
}

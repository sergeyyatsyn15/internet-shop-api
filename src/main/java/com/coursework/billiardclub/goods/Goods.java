package com.coursework.billiardclub.goods;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    private String name;
    private Double price;
    private String priceMeasure;
    private Long availableAmount;
    private String imageUrl;
}

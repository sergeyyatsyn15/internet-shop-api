package com.coursework.billiardclub.goods;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@CrossOrigin
@RequestMapping("/api/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @GetMapping("/{id}")
    public Goods getGoodsById(@PathVariable Long id) throws Exception {
        return goodsService.getOrderById(id);
    }

    @GetMapping
    public List<Goods> getAllGoods() {
        return goodsService.getAllGoods();
    }
}

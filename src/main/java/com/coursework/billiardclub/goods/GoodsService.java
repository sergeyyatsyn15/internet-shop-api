package com.coursework.billiardclub.goods;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public Goods getOrderById(Long id) throws Exception {
        return goodsRepository.findById(id).orElseThrow(Exception::new);
    }

    public List<Goods> getAllGoods() {
        return goodsRepository.findAll();
    }
}

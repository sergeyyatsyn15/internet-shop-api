SELECT 1;

CREATE TABLE client
(
    id          bigserial not null primary key,
    last_name   text      not null,
    first_name  text      not null,
    middle_name text      not null,
    address     text      not null,
    phone       text      not null,
    email       text      not null,
    regular     boolean   not null
);

CREATE TABLE sale
(
    id            bigserial not null primary key,
    client_id     bigint references client (id),
    sell_datetime timestamp not null,
    delivery_date timestamp not null,
    amount        bigint    not null
);

CREATE TABLE goods
(
    id               bigserial not null primary key,
    name             text      not null,
    price            float8    not null,
    price_measure    text      not null,
    available_amount bigint    not null,
    image_url        text      not null
);
